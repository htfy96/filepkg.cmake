# filepkg.cmake
根据依赖的文件自动查找并安装系统包管理器中的软件包

当前支持`pacman`和`apt`包管理器, 如果有需求欢迎提交patch.

![ScreenShot](http://git.oschina.net/htfy96/filepkg.cmake/raw/master/screenshot.gif)

## 入门使用

### 把filepkg.cmake放到CMake的模块目录下

如果你之前没有设置过`CMAKE_MODULE_PATH`, 那么下面这行代码将把`源文件目录/cmake/Modules`设为CMake的模块目录之一, 你也应该把`filepkg.cmake`放到其中:


```
set(CMAKE_MODULE_PATH ${CMAKE_MODULE_PATH} "${CMAKE_SOURCE_DIR}/cmake/Modules/")
```

### 引用并使用

```
include(filepkg)
INSTALL_FILE(/mysql/mysql.h)
# 注意: 最新版本不再需要前置的*了
```

如果这一步失败, `INSTALL_FILE_FAILED` 变量将被设为`1`

## 额外配置

当`INSTALL_FILE_NOCONFIRM` 变量被设置的时候, 软件包管理器安装前的确认将被跳过.

